<?php

use App\Jobs\SendNotificationJob;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/test', function () {
    $fcmToken = "c2iqjLR0Gdi2ohr0zwd8i7:APA91bHSNBgRLT1q1ElPzSqaiph5_UtEgNTPqPPOSDF-o5mTasEQxLMtKsv_DDjva0cVdtOI2NQoB29iY09KW2ojZHFEBsMF5RvYI9V9TiiaPl2IYReJxZEBqbBAMEYIivqQ9LEBbUaf";

    SendNotificationJob::dispatch($fcmToken, [
        'body'   => 'body',
        'title'  => 'title',
    ])->delay(now()->addSecond());
    return "done";
});
