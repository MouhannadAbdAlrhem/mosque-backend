<?php

use App\Enums\RoleType;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\StudentController;

Route::controller(StudentController::class)
    ->name('students.')
    ->prefix('students')
    ->group(function () {
        $superAdmin = (string)RoleType::SUPER_ADMIN->value;
        $admin = (string)RoleType::ADMIN->value;
        Route::get('/', 'index');
        Route::get('/top', 'getTopStudents');
        Route::post('/', 'store')->middleware("roles:$superAdmin,$admin");
        Route::put('/{user}', 'update')->middleware("roles:$superAdmin,$admin");
        Route::put('/{user}/level', 'updateStudentLevel')->middleware("roles:$superAdmin");
        Route::get('/{teacherId}', 'GetStudentsByTeacher');
    });
