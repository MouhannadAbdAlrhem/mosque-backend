<?php

use App\Enums\RoleType;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\SearchController;

Route::controller(SearchController::class)
    ->prefix('search')
    ->name('search.')
    ->middleware('roles:' . RoleType::SUPER_ADMIN->value)
    ->group(function () {
        Route::get('/students', 'students');
        Route::get('/admins', 'admins');
    });
