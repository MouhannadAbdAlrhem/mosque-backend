<?php

use App\Enums\RoleType;
use App\Http\Controllers\Api\V1\AuthController;
use Illuminate\Support\Facades\Route;


Route::controller(AuthController::class)
    ->group(function () {
        $superAdmin = (string)RoleType::SUPER_ADMIN->value;

        Route::post('/sign-up',  'signUp');
        Route::post('/sign-in',  'signIn');
        Route::middleware(['auth:api', 'tokenable:access'])->group(function () use ($superAdmin) {
            Route::post('/logout',  'logout');
            Route::delete('/{user}/ban',  'ban')->middleware("roles:$superAdmin");
            Route::put('/{user}/remove-ban',  'removeBan')->middleware("roles:$superAdmin");
            Route::put('/reset-password', 'resetPassword');

            Route::get('/refresh',  'refresh')
                ->withoutMiddleware('tokenable:access')
                ->middleware('tokenable:refresh');
        });
    });
