<?php

use App\Enums\RoleType;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\NoteController;

Route::prefix('notes')
    ->name('notes.')
    ->controller(NoteController::class)
    ->group(function () {
        $superAdmin = (string)RoleType::SUPER_ADMIN->value;
        $admin = (string)RoleType::ADMIN->value;
        Route::get('/{user}', 'index');
        Route::post('/', 'store')->middleware("roles:$superAdmin,$admin");
        Route::put('/{note}', 'update')->middleware("roles:$superAdmin,$admin");
        Route::delete('/{note}', 'destroy')->middleware("roles:$superAdmin,$admin");
    });
