<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\ProfileController;

Route::controller(ProfileController::class)
    ->prefix('profile')
    ->group(function () {
        Route::get('/', 'show');
        Route::put('/', 'update');
    });
