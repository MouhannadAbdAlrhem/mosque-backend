<?php

use App\Enums\RoleType;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\PresenceController;

Route::prefix('presences')
    ->name('presences.')
    ->controller(PresenceController::class)
    ->group(function () {
        $superAdmin = (string)RoleType::SUPER_ADMIN->value;
        Route::get('/{user}', 'getStudentPresences');
        Route::post('/', 'store')->middleware("roles:$superAdmin");
        Route::delete('/{presence}', 'destroy')->middleware("roles:$superAdmin");
    });
