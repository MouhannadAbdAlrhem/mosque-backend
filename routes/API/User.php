<?php

use App\Enums\RoleType;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\UserController;

Route::controller(UserController::class)
    ->name('users.')
    ->prefix('users')
    ->group(function () {
        $superAdmin = (string)RoleType::SUPER_ADMIN->value;
        $admin = (string)RoleType::ADMIN->value;
        Route::get('/', 'index')->middleware("roles:$superAdmin,$admin");
        Route::post('/', 'store')->middleware("roles:$superAdmin");
        Route::put('/{user}', 'update')->middleware("roles:$superAdmin");
    });
