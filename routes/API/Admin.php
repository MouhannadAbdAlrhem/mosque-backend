<?php

use App\Enums\RoleType;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\AdminController;

Route::controller(AdminController::class)
    ->name('admins.')
    ->prefix('admins')
    ->middleware("roles:" . (string)RoleType::SUPER_ADMIN->value)
    ->group(function () {
        Route::get('/', 'index');
        Route::post('/', 'store');
        Route::put('/{user}', 'update');
        Route::put('/{user}/reset-password', 'resetPassword');
    });
