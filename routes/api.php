<?php

use App\Enums\RoleType;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\ProfileController;
use App\Jobs\SendNotificationJob;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
//
include_once "API/Auth.php";
Route::name('api.')
    ->middleware(['auth:api', 'tokenable:access'])
    ->group(function () {
        include_once "API/Profile.php";
        include_once "API/Admin.php";
        include_once "API/Student.php";
        include_once "API/User.php";
        include_once "API/Note.php";
        include_once "API/Presence.php";
        include_once "API/Search.php";
    });
