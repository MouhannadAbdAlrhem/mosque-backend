<?php

use App\Enums\RoleType;
use App\Enums\StudentLevel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('phone')->nullable()->unique();
            $table->string('password')->nullable();
            $table->string('name');
            $table->date('birth_date');
            $table->string('location');
            $table->string('memorization')->nullable();
            $table->string('level')->default(StudentLevel::UNKNOWN);
            $table->string('role')->default(RoleType::USER);
            $table->string('fcm_token')->nullable();
            $table->string('contact_phone')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
