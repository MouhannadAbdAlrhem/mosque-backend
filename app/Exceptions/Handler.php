<?php

namespace App\Exceptions;

use Exception;
use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });


        $this->renderable(function (NotFoundHttpException $e, $request) {
            if ($request->is('api/*')) {
                return response()->json([
                    'status'    => false,
                    'error'     => $e->getMessage()
                ], $e->getStatusCode());
            }
        });

        $this->renderable(function (AccessDeniedHttpException $e, $request) {
            if ($request->is('api/*')) {
                return response()->json([
                    'status'    => false,
                    'error'     => $e->getMessage()
                ], $e->getStatusCode());
            }
        });

        $this->renderable(function (ValidationException $e, $request) {
            if ($request->is('api/*')) {
                return response()->json([
                    'status'    => false,
                    'message'   => 'Validation error',
                    'errors'    => collect($e->errors())->map(fn ($error) => $error[0])
                ], 422);
            }
        });

        $this->renderable(function (BadRequestException $e, $request) {
            if ($request->is('api/*')) {
                return response()->json([
                    'status'    => false,
                    'message'   => $e->getMessage(),
                ], 400);
            }
        });

        $this->renderable(function (Exception $e, $request) {
            if ($request->is('api/*') && ($e instanceof AuthenticationException)) {
                return response()->json([
                    'status'    => false,
                    'message'   => $e->getMessage()
                ], 401);
            }
        });




        $this->renderable(function (Exception $e, $request) {
            if ($request->is('api/*')) {
                return response()->json([
                    'status'    => false,
                    'message'   => $e->getMessage()
                ],  500);
            }
        });
    }
}
