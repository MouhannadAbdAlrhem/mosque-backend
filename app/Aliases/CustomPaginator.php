<?php

namespace App\Aliases;

use Illuminate\Pagination\LengthAwarePaginator;


class CustomPaginator extends LengthAwarePaginator
{

    public function toArray()
    {
        return [
            'items'         => $this->items->toArray(),
            'total'         => $this->total(),
            'count'         => $this->count(),
            'per_page'      => $this->perPage(),
            'current_page'  => $this->currentPage(),
            'totalPages'    => $this->lastPage(),
            ...(array)session('additionalResponse', [])
        ];
    }
}
