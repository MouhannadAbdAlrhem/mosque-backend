<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class NotificationService
{
    public static function send(string $fcmToken, array $payload)
    {
        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = env('FIREBASE_DASHBOARD_SERVER_KEY');

        $notification = [
            "title" => $payload['title'],
            "body"  => $payload['body'],
            "sound" => "default",
            "badge" => "1",
        ];

        $requestBody = [
            "to"            => $fcmToken,
            "notification"  => $notification,
            "data"          => $payload,
            "priority"      => "high",
        ];

        $headers = [
            "Content-Type: application/json",
            "Authorization: key=$serverKey"
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestBody));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //Send the request
        curl_exec($ch);
        //Close request
        // Http::withHeaders($headers)->post($url, $requestBody);
    }
}
