<?php

namespace App\Services;

class AuthService
{


    public static function createAccessTokenById(string $userId, array $claims = [])
    {
        return auth('api')->setTTL(config('app.access_token_ttl'))->claims(['type' => 'access', ...$claims])->tokenById($userId);
    }
    public static function createRefreshTokenById(string $userId, array $claims = [])
    {
        return auth('api')->setTTL(config('app.refresh_token_ttl'))->claims(['type' => 'refresh', ...$claims])->tokenById($userId);
    }
}
