<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

trait HasResponse
{
    public function success($data = [], ?string $message = null, int $status = Response::HTTP_OK, string $resName = "data", $append = []): JsonResponse
    {
        $response = ['status' => true, $resName => $data, ...$append];
        $message ? $response['message'] = $message : '';
        return response()->json($response, $status);
    }

    public function error(?string $message = null, int $status = Response::HTTP_NOT_FOUND): JsonResponse
    {
        $response = ['status' => false];
        if ($message) $response['message'] = $message;

        return response()->json($response, $status);
    }
}
