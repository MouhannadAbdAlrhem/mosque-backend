<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Enums\RoleType;
use App\Enums\StudentLevel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, SoftDeletes, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'phone',
        'contact_phone',
        'password',
        'birth_date',
        'location',
        'level',
        'memorization',
        'teacher_id',
        'points',
        'deleted_at',
        'role',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'level'         => StudentLevel::class,
        'role'          => RoleType::class,
        'password'      => 'hashed',
        'birth_date'    => 'date',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function booting()
    {
        static::addGlobalScope('deleted', fn (Builder $builder) => $builder->withTrashed());
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function scopeSuperAdmin($query)
    {
        return $query->where('role', RoleType::SUPER_ADMIN);
    }
    public function scopeAdmin($query)
    {
        return $query->where('role', RoleType::ADMIN);
    }
    public function scopeUser($query)
    {
        return $query->where('role', RoleType::USER);
    }
    public function scopeStudent($query)
    {
        return $query->where('role', RoleType::STUDENT);
    }

    public function scopeWeak($query)
    {
        return $query->where('level', StudentLevel::WEAK);
    }
    public function scopeMedium($query)
    {
        return $query->where('level', StudentLevel::MEDIUM);
    }
    public function scopeGood($query)
    {
        return $query->where('level', StudentLevel::GOOD);
    }
    public function scopeVeryGood($query)
    {
        return $query->where('level', StudentLevel::VERY_GOOD);
    }
    public function scopeExcellent($query)
    {
        return $query->where('level', StudentLevel::EXCELLENT);
    }
    public function scopeTop($query)
    {
        return $query->where('level', StudentLevel::TOP);
    }
    public function scopeUnknown($query)
    {
        return $query->where('level', StudentLevel::UNKNOWN);
    }
    public function notes(): HasMany
    {
        if ($this->isSuperAdmin() || $this->isAdmin()) {
            return $this->hasMany(Note::class, 'added_by');
        }
        return $this->hasMany(Note::class);
    }
    public function presences()
    {
        return $this->hasMany(Presence::class);
    }
    public function students(): HasMany
    {
        return $this->hasMany(self::class, 'teacher_id');
    }
    public function isSuperAdmin()
    {
        return $this->role === RoleType::SUPER_ADMIN;
    }
    public function isAdmin()
    {
        return $this->role === RoleType::ADMIN;
    }
    public function isUser()
    {
        return $this->role === RoleType::USER;
    }
    public function isStudent()
    {
        return $this->role === RoleType::STUDENT;
    }
    public function isBanned()
    {
        return $this->deleted_at !== null;
    }
    public function ban()
    {
        $this->delete();
    }
    public function removeBan()
    {
        $this->restore();
    }
}
