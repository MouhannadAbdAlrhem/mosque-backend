<?php

namespace App\Console\Commands;

use App\Enums\RoleType;
use App\Models\User;
use Exception;
use Illuminate\Console\Command;
use function Laravel\Prompts\text;
use function Laravel\Prompts\password;

class AddSuperAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:super {--name=} {--phone=} {--password=} {--birth-date=} {--location=}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add New Super Admin';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name           = $this->option('name');
        $phone          = $this->option('phone');
        $password       = $this->option('password');
        $birthDate      = $this->option('birth-date');
        $location       = $this->option('location');
        if (is_null($name)) {
            $name = text(
                label: 'Name?',
                hint: 'This will be displayed on your profile.',
                required: "The name is Required"
            );
        }

        if (is_null($phone) || strlen($phone) != 10) {
            $phone = text(
                label: 'Phone?',
                required: 'Your Phone is required.',
                validate: fn ($value) => match (true) {
                    strlen($value) != 10 => 'The phone must be 10 digits exactly.',
                    default => null
                },
            );
        }

        if (is_null($password) || strlen($password) < 8 || strlen($password) > 20) {
            $password = password(
                label: 'Password?',
                placeholder: 'password',
                hint: 'Minimum 8 characters \nMaximum 20 characters',
                required: "Your Password is Required",
                validate: fn (string $value) => match (true) {
                    strlen($value) < 8 => 'The password must be at least 8 characters.',
                    strlen($value) > 20 => 'The password must not exceed 20 characters.',
                    default => null
                }
            );
        }
        $regex = "/^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])$/";

        if (is_null($birthDate) || !((bool)preg_match($regex, $birthDate))) {
            $birthDate = text(
                label: 'Birth Date?',
                required: 'Your Birth Date is required.',
                hint: 'yyyy-mm-dd',
                validate: fn ($value) => match (true) {
                    (bool)!preg_match($regex, $value) => 'The Birth Date must be formatted like this yyyy-mm-dd',
                    default => null
                },
            );
        }
        if (is_null($location)) {
            $location = text(
                label: 'Location?',
                required: 'Your location is required.',
            );
        }
        try {
            $user = User::create([
                'phone'         => $phone,
                'name'          => $name,
                'password'      => $password,
                'birth_date'    => $birthDate,
                'location'      => $location,
                'role'          => RoleType::SUPER_ADMIN
            ]);
        } catch (Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
