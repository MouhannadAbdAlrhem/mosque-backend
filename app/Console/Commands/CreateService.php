<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class CreateService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create a new service class with the specified name';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        $serviceType =  $this->choice(
            'What kind of service do you want?',
            ['Services', 'DataTransferObjects', 'Enums'],
            'Services'
        );

        if ($serviceType == 'Services') {
            // Get the service type
            $type =  $this->choice(
                'What is the file type?',
                ['class', 'interface',],
                'class'
            );
            $serviceName = $this->argument('name') . '.php';
        } else if ($serviceType == 'DataTransferObjects') {
            $type =  $this->choice(
                'What kind of class type do you want?',
                ['class', 'readonly class'],
                'readonly class'
            );
            if (!str_contains($this->argument('name'), 'DTO')) {
                $serviceName = $this->argument('name') . 'DTO' . '.php';
            } else {
                $serviceName = $this->argument('name') . '.php';
            }
        } else {
            $type = 'enum';
            $returnType = $this->ask('What is return type of the enum');
            if (!in_array($returnType, ['int', 'string', 'float', 'bool', 'array'])) {
                $this->error('Wrong return type, please try agin');
                return;
            }
            $serviceName = $this->argument('name') . '.php';
        }
        // check if the service is already created
        $ServicesFolder = base_path() . '/app/' . $serviceType . '/' . $serviceName;

        if (File::exists($ServicesFolder)) {
            $this->error('This file is already created');
            return;
        }

        //create a file name and get name space and file name
        $names = $this->createFolders($ServicesFolder);

        $nameSpace = $names['nameSpace'];
        $classOrInterfaceName = $names['fileName'];


        /// create a file in path file
        $myFile = fopen($ServicesFolder, "w");
        // write in a file php tag , name space and file type
        if ($serviceType == 'Services') {
            $text = "<?php\nnamespace " . $nameSpace . "; \n\n" . $type . ' ' . $classOrInterfaceName . "\n{\n\n}";
        } else if ($serviceType == 'DataTransferObjects') {
            $text = "<?php\nnamespace " . $nameSpace . "; \n\n" . $type . ' ' . $classOrInterfaceName . "\n{\n"
                . "\tpublic function __construct()\n\t{\n\t}\n\n\n \tpublic static function fromRequest():self \n\t{ \n\t\t return new self();  \n\t} \n\n}";
        } else {
            $text = "<?php\nnamespace " . $nameSpace . "; \n\n" . $type . ' ' . $classOrInterfaceName . ":$returnType" . "\n{\n\n\n"
                . $this->enumFunctions() . "\n\n}";
        }


        fwrite($myFile, $text);
        fclose($myFile);

        // show Created successfully message
        $this->alert('Created successfully!');

        // print path to open created file
        dd('path =>  ' .  $nameSpace . '\\' . $classOrInterfaceName . '.php');
    }


    private function createFolders($path): array
    {

        $path = explode('/', $path);


        //save file name
        $classOrInterfaceName = $path[count($path) - 1];


        //get app index in array
        $key = array_search('app', $path);


        // delete file name
        unset($path[count($path) - 1]);

        // delete absolute path for system like:
        /*
            home/pcName/projectName/...../
        */
        $nameSpace = array_slice($path, $key, count($path) - 1);


        $subPath = base_path();

        //check if folders exists and cerate them
        foreach ($nameSpace as $key => $value) {
            $subPath .= '/' .  $value;
            if (File::exists($subPath)) continue;
            else File::makeDirectory($subPath);
        }

        ///rename app => App
        $key = array_search('app', $nameSpace);
        $nameSpace[$key] = 'App';

        $nameSpace = implode('\\', $nameSpace);
        $classOrInterfaceName = explode('.', $classOrInterfaceName)[0];
        return ['nameSpace' => $nameSpace, 'fileName' => $classOrInterfaceName];
    }

    public function enumFunctions(): string
    {
        return '    public static function matchEnum($value): self
    {
        return match ($value) {
        };
    }

    public static function formSelf(self $value): string
    {
        return match ($value) {
        };
    }

    public static function casesValue(): array
    {
        $cases = self::cases();
        return array_map(fn ($enum) => str()->lower($enum->name), $cases);
    }';
    }
}
