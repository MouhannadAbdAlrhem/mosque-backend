<?php

namespace App\Http\Controllers\Api\V1;

use Admin;
use App\Models\User;
use App\Enums\RoleType;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\DataTransferObjects\UserDTO;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\ProfileResource;
use App\Http\Requests\StoreAdminRequest;
use App\Http\Requests\UpdateAdminRequest;
use App\Http\Requests\ResetPasswordRequest;

class AdminController extends Controller
{
    public function index()
    {
        $admins = User::admin()->paginate();
        return $this->success(data: ProfileResource::collection($admins)->resource);
    }

    public function store(StoreAdminRequest $request)
    {
        $data = objectToArrayConverter(UserDTO::fromRequest($request));
        $data['role'] = RoleType::ADMIN;

        User::create($data);

        return $this->success(message: "Created Successfully", status: Response::HTTP_CREATED);
    }

    public function update(UpdateAdminRequest $request, User $user)
    {
        if (!$user->isAdmin()) {
            return $this->error(message: "This is not an admin", status: Response::HTTP_BAD_REQUEST);
        }
        $data = objectToArrayConverter(UserDTO::fromRequest($request));
        $user->update($data);

        return $this->success(message: "Admin Updated Successfully");
    }

    public function resetPassword(ResetPasswordRequest $request, User $user)
    {
        $user->password = $request->validated('password');
        $user->save();
        return $this->success('Password Updated Successfully');
    }
}
