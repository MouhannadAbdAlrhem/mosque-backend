<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\User;
use App\Enums\RoleType;
use Illuminate\Http\Request;
use App\Services\AuthService;
use Illuminate\Http\Response;
use App\DataTransferObjects\UserDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\SignInRequest;
use App\Http\Requests\SignUpRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Auth\Access\AuthorizationException;

class AuthController extends Controller
{
    public function signUp(SignUpRequest $request)
    {
        $userData = objectToArrayConverter(UserDTO::fromRequest($request));
        $userData['role'] = RoleType::USER;
        $user = User::create($userData);

        $accessToken  = AuthService::createAccessTokenById($user->id);
        $refreshToken = AuthService::createRefreshTokenById($user->id);

        $response = [
            'access'    => $accessToken,
            'refresh'   => $refreshToken,
            'role'      => $user->role,
        ];

        return $this->success(data: $response, status: Response::HTTP_CREATED);
    }

    public function signIn(SignInRequest $request)
    {
        $user = User::wherePhone($request->validated('phone'))->first();

        if ($user?->isBanned()) {
            throw new AuthorizationException("You Are Banned From Accessing This application");
        }
        if (isset($user) && Hash::check($request->validated('password'), $user->password)) {

            $accessToken  = AuthService::createAccessTokenById($user->id, ['role' => $user->role]);
            $refreshToken = AuthService::createRefreshTokenById($user->id, ['role' => $user->role]);

            $response = [
                'access'          => $accessToken,
                'refresh'         => $refreshToken,
                'role'            => $user->role,
                'phone'           => $user->phone,
                'name'            => $user->name,
                'birth_date'      => $user->birth_date?->format('Y-m-d'),
            ];
            return $this->success(data: $response);
        }
        return $this->error(message: "Invalid Credentials");
    }

    public function logout()
    {
        Auth::guard('api')->logout();

        return $this->success(message: "Logged Out Successfully");
    }

    public function refresh()
    {
        $user = Auth::user();

        $accessToken  = AuthService::createAccessTokenById($user->id, ['role' => $user->role]);
        $refreshToken = AuthService::createRefreshTokenById($user->id, ['role' => $user->role]);

        $response = [
            'access'    => $accessToken,
            'refresh'   => $refreshToken,
            'role'      => $user->role,
        ];
        return $this->success(data: $response);
    }


    public function resetPassword(ResetPasswordRequest $request)
    {
        $user = Auth::user();
        $user->password = $request->validated('password');
        $user->save();

        return $this->success('Password Updated Successfully');
    }

    public function ban(User $user)
    {
        $user->ban();

        return $this->success(message: "User Banned Successfully");
    }

    public function removeBan(User $user)
    {
        $user->removeBan();

        return $this->success(message: "Banned Removed Successfully");
    }
}
