<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\User;
use App\Enums\RoleType;
use Illuminate\Http\Response;
use App\DataTransferObjects\UserDTO;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProfileResource;
use App\Http\Requests\StoreAdminRequest;
use App\Http\Requests\UpdateAdminRequest;

class UserController extends Controller
{
    public function index()
    {
        $users = User::user()->paginate();
        return $this->success(data: ProfileResource::collection($users)->resource);
    }
    public function store(StoreAdminRequest $request)
    {
        $data = objectToArrayConverter(UserDTO::fromRequest($request));
        $data['role'] = RoleType::USER;

        User::create($data);

        return $this->success(message: "Created Successfully", status: Response::HTTP_CREATED);
    }

    public function update(UpdateAdminRequest $request, User $user)
    {
        if (!$user->isUser()) {
            return $this->error(message: "This is not a user", status: Response::HTTP_BAD_REQUEST);
        }
        $data = objectToArrayConverter(UserDTO::fromRequest($request));
        $user->update($data);

        return $this->success(message: "User Updated Successfully");
    }
}
