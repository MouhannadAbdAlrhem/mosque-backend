<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Note;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\DataTransferObjects\NoteDTO;
use App\Http\Controllers\Controller;
use App\Http\Resources\NoteResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreNoteRequest;
use App\Http\Requests\UpdateNoteRequest;
use Illuminate\Auth\Access\AuthorizationException;

class NoteController extends Controller
{
    public function index(User $user)
    {
        if ($user->isUser()) {
            return $this->error(message: "User model can't have notes", status: Response::HTTP_BAD_REQUEST);
        }
        $notes = $user->notes()->with('addedBy')->paginate();

        return $this->success(data: NoteResource::collection($notes)->resource);
    }

    public function store(StoreNoteRequest $request)
    {
        $data = objectToArrayConverter(NoteDTO::fromRequest($request));
        $user = User::findOrFail($data['user_id']);
        if (!$user->isStudent()) {
            return $this->error(message: "This is not a student", status: Response::HTTP_BAD_REQUEST);
        }

        $data['added_by'] = Auth::id();

        $user->notes()->create($data);

        return $this->success(message: "Note Added Successfully");
    }

    public function update(UpdateNoteRequest $request, Note $note)
    {
        if ($note->user_id !== Auth::id() && !Auth::user()->isSuperAdmin()) {
            throw new AuthorizationException("This action is unauthorized.");
        }

        $data = objectToArrayConverter(NoteDTO::fromRequest($request));

        $note->update($data);

        return $this->success(message: "Note Updated Successfully");
    }

    public function destroy(Note $note)
    {
        if ($note->user_id !== Auth::id() && !Auth::user()->isSuperAdmin()) {
            throw new AuthorizationException("This action is unauthorized.");
        }

        $note->delete();

        return $this->success(message: "Note Deleted Successfully");
    }
}
