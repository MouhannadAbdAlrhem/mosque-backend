<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProfileResource;

class SearchController extends Controller
{
    public function students()
    {
        $text = request()->query('search');

        $students = User::student()->whereAny(['phone', 'name'], 'LIKE', "%$text%")->get();

        return $this->success(data: ProfileResource::collection($students));
    }
    public function admins()
    {
        $text = request()->query('search');

        $students = User::admin()->whereAny(['phone', 'name'], 'LIKE', "%$text%")->get();

        return $this->success(data: ProfileResource::collection($students));
    }
}
