<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\User;
use App\Enums\RoleType;
use App\Enums\StudentLevel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\DataTransferObjects\UserDTO;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\ProfileResource;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class StudentController extends Controller
{
    public function index()
    {
        $students = User::student()->paginate();
        return $this->success(data: ProfileResource::collection($students)->resource);
    }

    public function store(StoreStudentRequest $request)
    {
        $data = objectToArrayConverter(UserDTO::fromRequest($request));
        $data['role'] = RoleType::STUDENT;

        User::create($data);

        return $this->success(message: "Student Created Successfully");
    }

    public function update(UpdateStudentRequest $request, User $user)
    {
        if (!$user->isStudent()) {
            return $this->error(message: "This is not a student", status: Response::HTTP_BAD_REQUEST);
        }
        if ($user->teacher_id != Auth::id() && !Auth::user()->isSuperAdmin()) {
            return $this->error(message: "This is not your student", status: Response::HTTP_BAD_REQUEST);
        }
        $data = objectToArrayConverter(UserDTO::fromRequest($request));

        $user->update($data);

        return $this->success(message: "Student Updated Successfully");
    }

    public function updateStudentLevel(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'level'             => ['required', Rule::in(StudentLevel::cases()), Rule::notIn([StudentLevel::UNKNOWN])],
        ], $request->all());

        if (!$user->isStudent()) {
            return $this->error(message: "This is not a student", status: Response::HTTP_BAD_REQUEST);
        }
        if ($user->teacher_id != Auth::id() && !Auth::user()->isSuperAdmin()) {
            return $this->error(message: "This is not your student", status: Response::HTTP_BAD_REQUEST);
        }
        $level = StudentLevel::matchEnum((int)$validatedData['level']);

        if ($level === StudentLevel::TOP) {
            $topStudentsCount = User::student()->top()->count();
            throw_if($topStudentsCount >= 3, new BadRequestException("There Are 3 Top Students Already."));
        }

        $user->update(['level' => $level]);

        return $this->success(message: "Student Updated Successfully");
    }

    public function getTopStudents()
    {
        $students = User::student()->top()->get();

        return $this->success(data: ProfileResource::collection($students));
    }

    public function GetStudentsByTeacher(string $teacherId)
    {
        $user = User::admin()->findOrFail($teacherId);

        return $this->success(data: ProfileResource::collection($user->students()->paginate())->resource);
    }
}
