<?php

namespace App\Http\Controllers\Api\V1;

use App\DataTransferObjects\UserDTO;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Resources\ProfileResource;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function show()
    {
        $user = Auth::user();

        return $this->success(data: ProfileResource::make($user));
    }

    public function update(UpdateProfileRequest $request)
    {
        $userData = objectToArrayConverter(UserDTO::fromRequest($request));

        Auth::user()->update($userData);

        return $this->success(message: "Updated Successfully");
    }
}
