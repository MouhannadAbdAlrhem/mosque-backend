<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\User;
use App\Enums\RoleType;
use App\Models\Presence;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Resources\PresenceResource;
use Illuminate\Support\Facades\Session;

class PresenceController extends Controller
{
    public function getStudentPresences(User $user)
    {
        $startDate = Carbon::parse(request()->query('start_date') ?? now()->subCenturies(100));
        $endDate = Carbon::parse(request()->query('end_date') ?? now());
        $daysCount = $endDate->diffInDays($startDate);

        if (!$user->isStudent()) {
            return $this->error(message: "This is not a student", status: Response::HTTP_BAD_REQUEST);
        }
        $presences = Presence::where('user_id', $user->id)
            ->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->orderBy('created_at')
            ->paginate();
        $presences = PresenceResource::collection($presences)->resource;

        $presencesCount = $presences->count();
        $absencesCount = $daysCount - $presencesCount;

        Session::flash('additionalResponse', ['presences_count' => $presencesCount, 'absences_count' => $absencesCount, 'days_count' => $daysCount]);
        return $this->success(data: $presences);
    }

    public function store(Request $request)
    {
        $data   = $request->validate(['user_id' => ['required', 'string']]);

        $user = User::whereId($data['user_id'])->student()->firstOrFail();

        if (!isset($user)) {
            return $this->error(message: "This is not a student", status: Response::HTTP_BAD_REQUEST);
        }

        $hasPresenceToday = Presence::where('user_id', $data['user_id'])
            ->whereDate('created_at', now()->today())
            ->exists();

        if ($hasPresenceToday) {
            return $this->error(message: "Student Already Has A Presence Today", status: Response::HTTP_BAD_REQUEST);
        }

        Presence::create($data);

        return $this->success(message: "Presence Added Successfully");
    }

    public function destroy(Presence $presence)
    {
        $presence->delete();

        return $this->success(message: "Presence Deleted Successfully");
    }
}
