<?php

namespace App\Http\Resources;

use App\Enums\RoleType;
use App\Enums\StudentLevel;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'             => $this->id,
            'name'           => $this->name,
            'phone'          => $this->phone,
            'contac_phone'   => $this->contact_phone,
            'birth_date'     => $this->birth_date?->format('Y-m-d'),
            'location'       => $this->location,
            'memorization'   => $this->when($this->role == RoleType::STUDENT, $this->memorization),
            'level'          => $this->when($this->role == RoleType::STUDENT, $this->level),
            'teacher_id'     => $this->when($this->role == RoleType::STUDENT, $this->teacher_id),
            'points'         => $this->when($this->role == RoleType::STUDENT, $this->points),
            'role'           => $this->role,
            'is_banned'      => $this->deleted_at !== null,
            'created_at'     => $this->created_at->format('Y-m-d'),
        ];
    }
}
