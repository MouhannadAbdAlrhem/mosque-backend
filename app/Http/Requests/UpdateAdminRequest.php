<?php

namespace App\Http\Requests;

use App\Enums\RoleType;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Auth::user()->role === RoleType::SUPER_ADMIN;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'phone'             => ['nullable', 'string', Rule::unique('users', 'phone')->ignore(request()->route('user')), 'digits:10'],
            'password'          => ['nullable', 'string', 'min:8', 'max:20'],
            'name'              => ['nullable', 'string'],
            'birth_date'        => ['nullable', 'date', 'date_format:Y-m-d'],
            'location'          => ['nullable', 'string'],
        ];
    }
}
