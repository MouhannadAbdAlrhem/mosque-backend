<?php

namespace App\Http\Requests;

use App\Enums\RoleType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class StoreNoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return in_array(Auth::user()?->role, [RoleType::ADMIN, RoleType::SUPER_ADMIN]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'user_id'   => ['required', 'string'],
            'title'     => ['required', 'string'],
            'body'      => ['required', 'string'],
        ];
    }
}
