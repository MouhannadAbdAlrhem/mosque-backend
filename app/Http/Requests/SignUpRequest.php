<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use \Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class SignUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return !Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'phone'             => ['required', 'string', 'unique:users,phone', 'digits:10'],
            'password'          => ['required', 'string', 'min:8', 'max:20'],
            'name'              => ['required', 'string'],
            'birth_date'        => ['required', 'date', 'date_format:Y-m-d'],
            'location'          => ['required', 'string'],
        ];
    }
}
