<?php

namespace App\Http\Requests;

use App\Enums\RoleType;
use App\Enums\StudentLevel;
use App\Rules\ShouldBeTeacher;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return in_array(Auth::user()?->role, [RoleType::ADMIN, RoleType::SUPER_ADMIN]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'contact_phone'     => ['nullable', 'string', 'digits:10'],
            'name'              => ['nullable', 'string'],
            'birth_date'        => ['nullable', 'date', 'date_format:Y-m-d'],
            'location'          => ['nullable', 'string'],
            'memorization'      => ['nullable', 'string'],
            'level'             => ['nullable', Rule::in(StudentLevel::cases()), Rule::notIn([StudentLevel::UNKNOWN, StudentLevel::TOP])],
            'teacher_id'        => ['required', new ShouldBeTeacher],
            'points'            => ['required', 'integer'],
        ];
    }
}
