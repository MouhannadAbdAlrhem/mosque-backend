<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'phone'             => ['required', 'string', Rule::unique('users','phone')->ignore(Auth::id())],
            'name'              => ['required', 'string'],
            'birth_date'        => ['required', 'date', 'date_format:Y-m-d'],
            'location'          => ['required', 'string'],
        ];
    }
}
