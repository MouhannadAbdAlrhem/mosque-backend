<?php

namespace App\Enums;

enum StudentLevel: int
{
    case WEAK = 1;
    case MEDIUM = 2;
    case GOOD = 3;
    case VERY_GOOD = 4;
    case EXCELLENT = 5;
    case TOP = 6;
    case UNKNOWN = 7;

    public static function matchEnum($value): self
    {
        return match ($value) {
            1           => self::WEAK,
            2           => self::MEDIUM,
            3           => self::GOOD,
            4           => self::VERY_GOOD,
            5           => self::EXCELLENT,
            6           => self::TOP,
            7           => self::UNKNOWN,
            default     => self::UNKNOWN
        };
    }

    public static function formSelf(self $value): string
    {
        return match ($value) {
            self::WEAK          => 1,
            self::MEDIUM        => 2,
            self::GOOD          => 3,
            self::VERY_GOOD     => 4,
            self::EXCELLENT     => 5,
            self::TOP           => 6,
            self::UNKNOWN       => 7
        };
    }

    public static function casesValue(): array
    {
        $cases = self::cases();
        return array_map(fn ($enum) => str()->lower($enum->name), $cases);
    }
}
