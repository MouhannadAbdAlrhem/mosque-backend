<?php

namespace App\Enums;

enum RoleType: int
{
    case SUPER_ADMIN = 1;
    case ADMIN = 2;
    case USER = 3;
    case STUDENT = 4;

    public static function matchEnum($value): self
    {
        return match ($value) {
            1 => self::SUPER_ADMIN,
            2 => self::ADMIN,
            3 => self::USER,
            4 => self::STUDENT,
        };
    }

    public static function formSelf(self $value): string
    {
        return match ($value) {
            self::SUPER_ADMIN   => 1,
            self::ADMIN         => 2,
            self::USER          => 3,
            self::STUDENT       => 4,
        };
    }

    public static function casesValue(): array
    {
        $cases = self::cases();
        return array_map(fn ($enum) => str()->lower($enum->name), $cases);
    }
}
