<?php


if (!function_exists('objectToArrayConverter')) {
    function objectToArrayConverter(object $object, $withNull = false): array
    {
        $array = (array)$object;
        if (!$withNull) {
            $array = array_filter($array, fn ($index) => $index !== null);
        }
        return $array;
    }
}
