<?php

namespace App\Providers;

use App\Aliases\CustomPaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\LengthAwarePaginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        app()->alias(CustomPaginator::class, LengthAwarePaginator::class);
        Model::shouldBeStrict();
    }
}
