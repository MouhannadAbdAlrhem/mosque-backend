<?php

namespace App\DataTransferObjects;

use App\Enums\StudentLevel;
use App\Http\Requests\SignUpRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class UserDTO
{
    public function __construct(
        public readonly ?string $phone,
        public readonly ?string $password,
        public readonly ?string $name,
        public readonly ?string   $birth_date,
        public readonly ?string $location,
        public readonly ?StudentLevel $level,
        public readonly ?string $memorization,
        public readonly ?string $contact_phone,
        public readonly ?string $teacher_id,
        public readonly ?string $points,
    ) {
    }


    public static function fromRequest(Request $request): self
    {
        return new self(
            phone: $request->validated('phone'),
            password: $request->validated('password'),
            name: $request->validated('name'),
            birth_date: $request->validated('birth_date'),
            location: $request->validated('location'),
            level: StudentLevel::matchEnum((int)$request->validated('level')),
            memorization: $request->validated('memorization'),
            contact_phone: $request->validated('contact_phone'),
            teacher_id: $request->validated('teacher_id'),
            points: $request->validated('points'),
        );
    }
}
