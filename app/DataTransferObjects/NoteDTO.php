<?php

namespace App\DataTransferObjects;

use Illuminate\Http\Request;

class NoteDTO
{
    public function __construct(
        public ?string $user_id,
        public ?string $title,
        public ?string $body,
    ) {
    }


    public static function fromRequest(Request $request): self
    {
        return new self(
            user_id:    $request->validated('user_id'),
            title:      $request->validated('title'),
            body:       $request->validated('body'),
        );
    }
}
