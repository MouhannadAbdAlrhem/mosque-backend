// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/8.10.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.10.1/firebase-messaging.js');


firebase.initializeApp({

    apiKey: "AIzaSyB6dKNHVgEiFPV_hGuJbHyMgyAQqfMuvyw",

    authDomain: "mosque-efbeb.firebaseapp.com",

    projectId: "mosque-efbeb",

    storageBucket: "mosque-efbeb.appspot.com",

    messagingSenderId: "1023741724075",

    appId: "1:1023741724075:web:b8a9cc0749a2c3ecdb7eee",

    measurementId: "G-KVDM8GMZF6"

  }
);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();



messaging.onBackgroundMessage((payload) => {
    const notificationTitle = payload.notification.title;

    const notificationOptions = {
        body: payload.notification.body,
        // icon: window.location.origin + '/assets/images/NAZIK_SVG.svg',
    };

    self.registration.showNotification(notificationTitle, notificationOptions);

});
